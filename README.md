# Fruit Slots

【練習】【2016-07-17】
撰寫簡易水果機，使用HTML、CSS、JavaScript以及jQuery
練習`setTimeout`配合`Math.random()`，在時間內亂數選種水果機的水果。

預覽圖如下：
![](https://gitlab.com/w860403/fruit-slots/raw/master/docs/fruit-slots.PNG)